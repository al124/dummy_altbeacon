# SmartAttendance

SmartAttendance è il modulo aggiuntivo per la simulazione e l'emissione degli Altbeacon. Esso è in continua evoluzione e può essere soggetto a modifiche nel tempo.

Esso quindi permette di settare ed inviare un pacchetto dati BLE.

# Classi

| **Classi** | **Descrizione** |
|:-------------|:-------------
| **AltBeacon**      | Un Altbeacon "simulato", che può essere settato come si desidera
| **BroadcasterBLE** | Emette un paccheto BLE i cui settagi dipendono dalla classe AltBeacon
| **ByteTools**      | Utility per la lavorazione dei byte


# AltBeacon

Simula il settaggio di un AltBeacon fisico, tramite software

## Costruttori

| **Costruttore**  | **Tipo parametri** | **Descrizione** | 
|:-------------|:-------------|:-------------
| **AltBeacon()**                   | Nessuno             | Crea un Altbeacon con dei valori di default                                   |
| **AltBeacon(uuid)**               | UUID                | Crea un Altbeacon con l'UUID fornito in input                                 |
| **AltBeacon(uuid,major,minor)**   | UUID, int, int        | Crea un Altbeacon con l'UUID, major e minr forniti in input                   |


## Attributi

Per maggiori informazioni su questi campi vedere [qui](https://github.com/AltBeacon/spec) oppure l'immagine sottostante per farsi una rapida idea:

![pda2](/uploads/ce20258705d65ca589dd689a9202575f/pda2.png)

| **Attributi**  | **Tipo** | **Descrizione** | 
|:-------------|:-------------|:-------------
| **MANUFACTURER_PACKET_SIZE** | int    | Dimensione parte dati AltBeacons                                      |
| **BEACON_CODE**              | short  | Identificativo del tipo di Beacon, di deafult è quello dell'AltBeacon |
| **manufactureid**            | int    | Identificativo della compagnia, di default è quello di test           |
| **uuid**                     | UUID   | UUID, indica il numero di serie del dispositivo simulato              |
| **major**                    | int    | Valore a piacere tra 0 e 65536                                        |
| **minor**                    | int    | Valore a piacere tra 0 e 65536                                        |
| **manufacturedReserved**     | String | Campo riservato a funzioni dedicate alla compagnia                    |
| **power**                    | byte   | RSSI a 1m di sistanza                                                 |

## Metodi

| **Metodo**  | **Descrizione** | 
|:-------------|------------- 
| **public UUID getBeaconNamespace()**                                 | Restiruisce l'attuale uuid                                                                                |
| **public void setBeaconNamespace(UUID uuid)**                        | Setta l'uuid                                                                                              |
| **public int getMajor()**                                            | Restiruisce l'attuale major                                                                               |
| **public void setMajor(int major)**                                  | Setta il major                                                                                            |
| **public int getMinor()**                                            | Restiruisce l'attuale minor                                                                               |
| **public void setMinor(int minor)**                                  | Setta il minor                                                                                            |
| **public int getManufacturerId()**                                   | Restiruisce l'attuale manufactureid                                                                       |
| **public void setManufacturerId(int manufacturerId)**                | Setta il manufactorid                                                                                     |
| **public String getManufacturerReserved()**                          | Restiruisce l'attuale manufactureReserved                                                                 |
| **public void setManufacturerReserved(String manufacturerReserved)** | Setta il manufactureReserved                                                                              |
| **public byte getPower()**                                           | Restiruisce l'attuale power                                                                               |
| **public void setPower(byte power)**                                 | Setta il valore power                                                                                     |
| **public AdvertiseData generateAdvertiseData()**                     | Genera un AdvertiserData a partire dai settaggi agli attributi definiti. Vedi [qui](https://developer.android.com/reference/android/bluetooth/le/AdvertiseData) cosa è un AdvetiserData |

# BroadcasterBLE

Emette gli AdvertiserBLE, che non sono altro che dei pacchetti dati la cui parte dati è definita dalla classe AltBeacon.

Si compone di:
-   Una parte in grado di inviare pacchetti dati tramite l'istanza della classe **BluetoothLeAdvertiser** (emettitore)
-   Il metodo **advertise()** che non fa altro che iniziare a trasmettere i pacchetti BLE
-   Il metodo **stopAdvertise()** che non ferma il processo di emissione dei pacchetti BLE

## Costruttori

| **Costruttore**  | **Tipo parametri** | **Descrizione** | 
|:-------------|:-------------|:-------------
| **BroadcasterBLE()()**                   | Nessuno             | Crea un BroadcasterBLE in grado di emettere un AltBeacon con i valori uuid,major,minor di default     |
| **BroadcasterBLE(uuid)**                 | UUID                | Crea un BroadcasterBLE in grado di emettere un AltBeacon con l'uuid fornito, major e minor di default |
| **BroadcasterBLE(uuid,major,minor)**     | UUID, int, int      | Crea un BroadcasterBLE in grado di emettere un AltBeacon con l'uuid, major e minor forniti in input   |


## Attributi

| **Attributi**  | **Tipo** | **Descrizione** | 
|:-------------|:-------------|:-------------
| **uuid**                       | UUID                  | UUID, indica il numero di serie del dispositivo simulato                                                                                                                                                   |
| **major**                      | int                   | Valore a piacere tra 0 e 65536                                                                                                                                                                             |
| **minor**                      | int                   | Valore a piacere tra 0 e 65536                                                                                                                                                                             |
| **advertiser**                 | BluetoothLeAdvertiser | Crea un AdvertiserBLE in grado di emettere pacchetti dati BLE. Vedi la documentazione Android [qui](https://developer.android.com/reference/android/bluetooth/le/BluetoothLeAdvertiser)                    |
| **data**                       | AdvertiseData         | Crea la parte dedicata allAltbeacon del pacchetto dati BLE, da passare all'AdvertiserBLE. Vedi la documentazione Android [qui](https://developer.android.com/reference/android/bluetooth/le/AdvertiseData) |                                               |

## Metodi

Per maggiori informazioni dei metodi e degli oggetti citati qui sotto, potete vedere la documentazione Android completa [qui](https://developer.android.com/reference/android/bluetooth/le/BluetoothLeAdvertiser)
 
| **Metodo**  | **Descrizione** | 
|:-------------|------------- 
| **private AdvertiseCallback callbackStatus()**                       | Restituisce lo status del 'azione di emissione o stop , con eventuale messaggio di errore                                                                                                      |
| **public void advertise())**                                         | Emette i pacchetti dati BLE. Sfrutta il metodo advertiser.startAdvertising(...) che necessita almeno di un AdvertiseSettings, un AdveriseData e un AdveriseCallBack per iniziare a trasmettere |
| **public public void stopAdvertise()**                               | Stoppa l'emissione dei pacchetti dati BLE.                                                                                                                                                     |
| **public void setAltBeaconId(UUID uuid,int major,int minor)**        | Setta i valori di uuid, major e minor di un nuovo AltBeacon                                                                                                                                    |                 
| **public void setAltBeaconIdTEST(UUID uuid,int major,int minor)**    | Setta i valori di uuid, major e minor di un nuovo AltBeacon per i svolgere test di cambio id con valori giocattolo                                                                             |                 
# ByteTool (utility)

Fornisce una serie di strumenti di lavoro sui Byte e dei controlli sul corretto formato dei campi dell'Altbeacon
