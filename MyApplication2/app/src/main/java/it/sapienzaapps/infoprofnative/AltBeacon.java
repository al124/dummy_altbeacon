package it.sapienzaapps.infoprofnative;


import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;

import java.nio.ByteBuffer;
import java.util.UUID;

public class AltBeacon {
    //AD Lenght
    public final static int MANUFACTURER_PACKET_SIZE = 24;
    //Beacon Code for AltBeacon
    public final static short BEACON_CODE = (short)0xBEAC;
    //MFG ID [2 bytes]
    private int manufacturerId;
    //Beacon ID [UUID:16bytes][min/maj: 2bytes each one]
    private UUID uuid;
    private int major;
    private int minor;
    //MFG RSVD [1 Byte reserved for each]
    private String manufacturerReserved;
    //ref RSSI [value: from 0 to -127]
    private byte power;

    public AltBeacon() {
        setManufacturerId(0xFFFF);
        setBeaconNamespace(UUID.randomUUID());
        setMajor(4242);
        setMinor(4242);
        setPower((byte)-69);
        setManufacturerReserved("00");
    }

    public AltBeacon(UUID uuid) {
        setManufacturerId(0xFFFF);
        setBeaconNamespace(uuid);
        setMajor(123);
        setMinor(142);
        setPower((byte)-69);
        setManufacturerReserved("00");
    }

    public AltBeacon(UUID uuid,int major,int minor) {
        setManufacturerId(0xFFFF);
        setBeaconNamespace(uuid);
        setMajor(major);
        setMinor(minor);
        setPower((byte)-69);
        setManufacturerReserved("00");
    }

    public UUID getBeaconNamespace() {
        return uuid;
    }

    public void setBeaconNamespace(UUID uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = ByteTools.capToUnsignedShort(major);
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = ByteTools.capToUnsignedShort(minor);
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = ByteTools.capToUnsignedShort(manufacturerId);
    }

    public String getManufacturerReserved() {
        return manufacturerReserved;
    }


    public void setManufacturerReserved(String manufacturerReserved) {
        if (manufacturerReserved.length() > 2) {
            this.manufacturerReserved = "00";
            return;
        }
        this.manufacturerReserved = manufacturerReserved;
    }

    public byte getPower() {
        return power;
    }

    public void setPower(byte power) {
        this.power = power;
    }

    public AdvertiseData generateAdvertiseData() {
        /* When manufacturer reserved value is greater than 127, it cannot be
        converted to a byte. Hence a first conversion to int, then a cast to remove
        excessive bits. */
        final byte manufacturerReserved = (byte)Integer.parseInt(getManufacturerReserved(), 16);
        final ByteBuffer buffer = ByteBuffer.allocate(MANUFACTURER_PACKET_SIZE);
        buffer.putShort(BEACON_CODE);
        buffer.putLong(getBeaconNamespace().getMostSignificantBits());
        buffer.putLong(getBeaconNamespace().getLeastSignificantBits());
        buffer.put(ByteTools.toShortInBytes_BE(getMajor()));
        buffer.put(ByteTools.toShortInBytes_BE(getMinor()));
        buffer.put(getPower());
        buffer.put(manufacturerReserved);
        return new AdvertiseData.Builder()
                .addManufacturerData(getManufacturerId(), buffer.array())
                .build();
    }
}