package it.sapienzaapps.infoprofnative;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;

import android.app.Activity;
import android.util.Log;

import java.util.UUID;

public class BroadcasterBLE {
    private UUID uuid;
    private int major;
    private int minor;
    private BluetoothLeAdvertiser advertiser;
    private AdvertiseData data;
    
    public BroadcasterBLE(){
        this.major = 1000;
        this.minor = 1000;
        this.advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
        AltBeacon altbeacon = new AltBeacon();
        this. uuid = altbeacon.getBeaconNamespace();
        this.data = altbeacon.generateAdvertiseData();
    }

    public BroadcasterBLE(UUID uuid){
        this.uuid = uuid;
        this.advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
        this.data = new AltBeacon(uuid).generateAdvertiseData();
    }

    public BroadcasterBLE(UUID uuid, int major, int minor){
        this.uuid = uuid;
        this.advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
        this.data = new AltBeacon(uuid, major, minor).generateAdvertiseData();
    }

    private AdvertiseCallback callbackStatus(){
        AdvertiseCallback advertisingCallback = new AdvertiseCallback() {
            @Override
            public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                super.onStartSuccess(settingsInEffect);
            }

            @Override
            public void onStartFailure(int errorCode) {
                Log.e( "BLE", "Advertising onStartFailure: " + errorCode );
                super.onStartFailure(errorCode);
            }
        };
        return advertisingCallback;
    }

    public void advertise() {
        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode( AdvertiseSettings.ADVERTISE_MODE_LOW_POWER )
                .setTxPowerLevel( AdvertiseSettings.ADVERTISE_TX_POWER_LOW )
                .setConnectable(false)
                .build();
        advertiser.startAdvertising( settings, data, this.callbackStatus() );
    }

    public void stopAdvertise(){
        this.advertiser.stopAdvertising(this.callbackStatus());
    }

    public void setAltBeaconId(UUID uuid,int major,int minor){
        AltBeacon altbeacon = new AltBeacon(uuid,major,minor);
        this.data = altbeacon.generateAdvertiseData();
    }
}
