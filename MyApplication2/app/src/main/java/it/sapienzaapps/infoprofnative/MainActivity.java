package it.sapienzaapps.infoprofnative;

import androidx.appcompat.app.AppCompatActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.advertise_btn);
        Button stop = (Button) findViewById(R.id.advertise_btn_stop);
        final BroadcasterBLE beacon = new BroadcasterBLE();
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                beacon.advertise();
                beacon.stopAdvertise();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                beacon.stopAdvertise();
            }
        });
    }
}
